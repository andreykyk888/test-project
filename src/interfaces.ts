export interface IBuildingPosition {
  positionX: number;
  positionY: number;
}

export interface IHouse {
  isSet: boolean;
  positionX: number;
  positionY: number;
}

export interface IPowerStation extends IHouse {
  connectedHouses: IHouse[];
  isWorking: boolean;
}

