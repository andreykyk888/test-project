import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-scheme-builder-house-block',
  templateUrl: './scheme-builder-house-block.component.html',
  styleUrls: ['./scheme-builder-house-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchemeBuilderHouseBlockComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
