import {ChangeDetectionStrategy, Component} from '@angular/core';
import {IHouse, IPowerStation} from "../../../interfaces";
import {BehaviorSubject} from "rxjs";
import {TRANSMITTER_POSITION} from "./shared/transmitter-position/transmitter-position.token";
import {transmitterPosition$} from "./shared/transmitter-position/transmitter-position.const";

@Component({
  selector: 'app-scheme-builder',
  templateUrl: './scheme-builder.component.html',
  styleUrls: ['./scheme-builder.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [{
    provide: TRANSMITTER_POSITION,
    useValue: transmitterPosition$
  }]
})
export class SchemeBuilderComponent {
  powerStationArray = new BehaviorSubject<IPowerStation[]>([])
  houseArray = new BehaviorSubject<IHouse[]>([])

  constructor() { }

}
