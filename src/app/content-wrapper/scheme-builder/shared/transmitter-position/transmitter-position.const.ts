import {Subject} from "rxjs";
import {IBuildingPosition} from "../../../../../interfaces";

export const transmitterPosition$ = new Subject<IBuildingPosition>()
