import {InjectionToken} from "@angular/core";
import {Subject} from "rxjs";
import {IBuildingPosition} from "../../../../../interfaces";

export const TRANSMITTER_POSITION = new InjectionToken<Subject<IBuildingPosition>>('position.config')
