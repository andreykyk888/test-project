import {ChangeDetectionStrategy, Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {Observable, Subject} from "rxjs";

@Component({
  selector: 'app-scheme-toolbar',
  templateUrl: './scheme-toolbar.component.html',
  styleUrls: ['./scheme-toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchemeToolbarComponent implements OnInit {
  createBuildingType$ = new Subject<'station'|'house'|null>()
  constructor() { }

  ngOnInit(): void {
  }

  onCreateBuilding(buildingType: 'station'|'house'|null) {
    console.log(buildingType)
    this.createBuildingType$.next(buildingType)
  }

}
