import {Directive, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[appEmitterForCreateBuilding]'
})
export class EmitterForCreateBuildingDirective {
  @Input() typeCreatingBuilding: 'station'|'house'
  @Output() onCreateBuilding = new EventEmitter<'station'|'house'|null>()

  @HostListener('mousedown', ['$event'])
  onMouseDown(event: MouseEvent) {
    event.preventDefault()
    this.onCreateBuilding.emit(this.typeCreatingBuilding)
  }

  @HostListener('window:mouseup')
  onMouseUp(){
    console.log(1)
    this.onCreateBuilding.emit(null)
  }

}
