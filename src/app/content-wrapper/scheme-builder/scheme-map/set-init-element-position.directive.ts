import {Directive, ElementRef, Host, HostListener, Input, OnInit, Renderer2} from '@angular/core';
import {IHouse, IPowerStation} from "../../../../interfaces";

@Directive({
  selector: '[appSetInitPosition]'
})
export class SetInitElementPositionDirective implements OnInit {
  @Input() building: IHouse|IPowerStation

  constructor(
    private readonly render: Renderer2,
    private readonly element: ElementRef
  ) { }

  ngOnInit() {
    this.render.setStyle(this.element.nativeElement, 'top', `${this.building.positionY}px`)
    this.render.setStyle(this.element.nativeElement, 'left', `${this.building.positionX}px`)
  }
}
