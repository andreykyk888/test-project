import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {IHouse, IPowerStation} from "../../../../interfaces";

@Component({
  selector: 'app-scheme-map',
  templateUrl: './scheme-map.component.html',
  styleUrls: ['./scheme-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchemeMapComponent implements OnInit {
  @Input() powerStationArray = new Observable<IPowerStation[]>()
  @Input() houseArray = new Observable<IHouse[]>()

  ngOnInit(): void {
  }

}
