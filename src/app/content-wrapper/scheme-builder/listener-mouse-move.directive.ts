import {Directive, Host, HostListener, Inject} from '@angular/core';
import {TRANSMITTER_POSITION} from "./shared/transmitter-position/transmitter-position.token";
import {Subject} from "rxjs";
import {IBuildingPosition} from "../../../interfaces";

@Directive({
  selector: '[listenerMouseMove]'
})
export class ListenerMouseMoveDirective {

  constructor(
    @Host() @Inject(TRANSMITTER_POSITION) private transmitterPosition$: Subject<IBuildingPosition>
  ) { }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    const positionObj: IBuildingPosition = {
      positionX: event.clientX,
      positionY: event.clientY
    }
    this.transmitterPosition$.next(positionObj)
  }
}
