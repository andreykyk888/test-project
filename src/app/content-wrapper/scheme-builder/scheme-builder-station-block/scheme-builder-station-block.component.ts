import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-scheme-builder-station-block',
  templateUrl: './scheme-builder-station-block.component.html',
  styleUrls: ['./scheme-builder-station-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchemeBuilderStationBlockComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
