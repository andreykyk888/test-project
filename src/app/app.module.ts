import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SchemeBuilderComponent } from './content-wrapper/scheme-builder/scheme-builder.component';
import { SchemeToolbarComponent } from './content-wrapper/scheme-builder/scheme-toolbar/scheme-toolbar.component';
import { ContentWrapperComponent } from './content-wrapper/content-wrapper.component';
import { SchemeMapComponent } from './content-wrapper/scheme-builder/scheme-map/scheme-map.component';
import { SetInitElementPositionDirective } from './content-wrapper/scheme-builder/scheme-map/set-init-element-position.directive';
import { EmitterForCreateBuildingDirective } from './content-wrapper/scheme-builder/scheme-toolbar/emitter-for-create-building.directive';
import { SchemeBuilderHouseBlockComponent } from './content-wrapper/scheme-builder/scheme-builder-house-block/scheme-builder-house-block.component';
import { SchemeBuilderStationBlockComponent } from './content-wrapper/scheme-builder/scheme-builder-station-block/scheme-builder-station-block.component';
import { ListenerMouseMoveDirective } from './content-wrapper/scheme-builder/listener-mouse-move.directive';

@NgModule({
  declarations: [
    AppComponent,
    SchemeBuilderComponent,
    SchemeToolbarComponent,
    ContentWrapperComponent,
    SchemeMapComponent,
    SetInitElementPositionDirective,
    EmitterForCreateBuildingDirective,
    SchemeBuilderHouseBlockComponent,
    SchemeBuilderStationBlockComponent,
    ListenerMouseMoveDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
